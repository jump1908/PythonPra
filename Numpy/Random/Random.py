import numpy as np
import numpy.random as random

# 设置随机数种子，seed返回值为None
# 当我们设置相同的seed时，每次生成的随机数相同
random.seed(42)
# print(random.rand(4))
random.seed(42)
# print(random.rand(4))

# 产生一个1*3，(0, 1)之间的浮点随机数
# array[[0.37454012, 0.95071431, 0.73199394]]
# 后面的例子就不在注释中给出具体结果了
a = random.rand(1, 3)
# print(a)

# 产生一个[0, 1)之间的浮点型随机数
b = random.random()
# print(b)

# 下边4个没有区别，都是按照指定大小产生[0.1)之间的浮点型随机数array，
c = random.random((3, 3))
d = random.sample((3, 3))
e = random.random_sample((3, 3))
f = random.ranf((3, 3))
# print(c, "\n", d, "\n",  e, "\n", f)

# 产生一个[1. 6)之间的浮点型随机数
g = 5*random.random(10)+1
h = random.uniform(1, 6, 10)
# print(g, "\n", h)

# 产生10个[1. 6)之间的整形随机数
i = random.randint(1, 6, 10)
# print(i)

# 产生2*5的标准正太分布样本
j = random.normal(size=(5, 2))
# print(j)

# 产生5个，n=5，p=0.5的二项分布样本
k = random.binomial(n=5, p=0.5, size=5)
# print(k)

randa = np.arange(10)

# 从randa中有回放的随机采样7个，[6 6 7 4 2 7 5] 有重复
l = random.choice(randa, 7)
# print(l, "\n", randa)

# 从randa中无回放的随机采样7个，[6 1 3 8 7 5 4] 无重复
m = random.choice(randa, 7, replace=False)
# print(m, "\n", randa)

# 对randa进行乱序并返回一个新的array
n = random.permutation(randa)
# print(n)

# 对randa进行in-place乱序，这个函数会改变randa
random.shuffle(randa)
# print(randa)

# 生成一个长度为9的随机bytes序列并作为str返回
# '\x8e\x063\x8a\xaa\xcck\xc1\x1c
o = random.bytes(9)
print(o)