import numpy as np

"""
array([ [[ 0,  1,  2,  3],
        [ 4,  5,  6,  7],
        [ 8,  9, 10, 11]],
        [[12, 13, 14, 15],
        [16, 17, 18, 19],
        [20, 21, 22, 23]]])
"""
a = np.arange(24).reshape((2, 3, 4))
b = a[1][1][1]                          #17

"""
array([ [ 8,  9, 10, 11],
        [20, 21, 22, 23]])
"""
c = a[:, 2, :]

"""
用...表示没有明确指出的维度
array([ [ 1  5  9]
        [13 17 21]])
"""
e = a[..., 1]

"""
-1表示从尾部数第一个
array([ [[ 5,  6]
        [ 9, 10]],
        [[17, 18]
        [21, 22]]])
"""
e = a[:, 1:, 1:-1]

"""
按照下标位置进行划分
[array([0, 1, 2]), array([3, 4, 5]), array([6, 7, 8])]
"""
g = np.split(np.arange(9), 3)

"""
按照下标位置进行划分
[array([0, 1]), array([2, 3, 4, 5]), array([6, 7, 8])]
"""
h = np.split(np.arange(9), [2, -3])

l0 = np.arange(6).reshape((2, 3))
l1 = np.arange(6, 12).reshape((2, 3))

"""
vstack是指沿着纵轴拼接两个array，vertical
hstack是指沿着横轴拼接两个array，horizontal
更广义的拼接用concatenate实现
stack不是拼接而是在输入array的基础上增加一个新的维度
"""
m = np.vstack((l0, l1))
p = np.hstack((l0, l1))
q = np.concatenate((l0, l1))
r = np.concatenate((l0, l1), axis=-1)
s = np.stack((l0, l1))