import numpy as np

# 绝对值，-1
a = np.abs(-1)
# print(a)

# sin函数，1.0
b = np.sin(np.pi/2)

# tanh逆函数，0.5000010715784053
c = np.arctanh(0.462118)
# print(c)

# e为低的指数函数，20.085536923187668
d = np.exp(3)
# print(d)

# 2的3次方，8
f = np.power(2, 3)

# 点积， 1*3+2*3=11
g = np.dot([1, 2], [3, 4])
# print(g)

# 平均值
m = np.mean([4, 5, 6, 7])

# 标准差，0.9682458365518543
p = np.std([1, 2, 3, 2, 1, 3, 2, 0])
print(p)