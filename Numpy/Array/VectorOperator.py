import numpy as np

a = np.array([
    [1, 2, 3],
    [4, 5, 6]
])

b = np.array([
    [1, 2, 3],
    [1, 2, 3]
])

"""
维度一样的array，对位计算
array([ [ 2,  4, 6],
        [ 5,  7, 9]])
"""
print(a+b)

"""
array([ [[0, 0, 0],
        [3, 3, 3]])
"""
print(a-b)

"""
array([ [[ 1,  4,  9],
        [ 4, 10, 18]])
"""
print(a*b)

"""
array([ [[1.  1.  1. ]
        [4.  2.5 2. ]])
"""
print(a/b)

"""
array([ [[ 1  4  9]
        [16 25 36]])
"""
print(a ** 2)


"""
广播机制
"""
c = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12]
])
d = np.array([2, 2, 2])

"""
广播机制让计算的表达式保持简洁
d和c的每一行分别进行运算
array([ [[ 3,  4,  5],
        [ 6,  7,  8],
        [ 9, 10, 11],
        [12, 13, 14]])
"""
print(c+d)

"""
array([ [[ 2,  4,  6],
        [ 8, 10, 12],
        [14, 16, 18],
        [20, 22, 24]])
"""
print(c*d)

"""
array([ [[ 0,  1,  2],
        [ 3,  4,  5],
        [ 6,  7,  8],
        [ 9, 10, 11]])
"""
print(c-1)