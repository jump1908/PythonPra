import numpy as np

c = [[1, 2], [3, 4]]                # 二维数组
d = np.array(c)                     # 二维数组的创建
print(d.shape)                      # 二维数组的大小
print(d.size)                       # 二维数组元素数
print(d.max(axis=0))                # 找维度0，也就是最后一个维度上的最大值，array([3, 4])
print(d.max(axis=1))                # 找维度1，也就是倒数第二个维度上的最大值，array([2, 4])
print(d.mean(axis=0))               # 找维度0，也就是一个维度上的均值，array([2., 3.])
print(d.flatten())                  # 展开一个numpy数组为1维数组，array([1, 2, 3, 4])
print(np.ravel(c))                  # 展开一个可以解析的结构为1，array([1, 2, 3, 4])

# 3*3的浮点型2维数组，并且初始化所有元素值为1
e = np.ones((3, 3), dtype=np.float)
print(e)

# 创建一个一维数组，元素值是把3重复4次，array([3, 3, 3, 3])
f = np.repeat(3, 4)
print(f)

# 2*2*3的无符号8位整形3维数组，并且初始化所有元素值为0
g = np.zeros((2, 2, 3), dtype=np.uint8)
print(g.shape)                          # (2, 2, 3)
h = g.astype(np.float)                  # 用另一种类型表示

l = np.arange(10)                       # 有序数组，array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print(l)
m = np.linspace(0, 6, 5)                # 等差数列，0到6之间前5个取值，array([0.,  1.5, 3.,  4.5, 6. ])
print(m)

p = np.array([[1, 2, 3, 4],
              [4, 5, 6, 7]])
np.save('p.npy', p)                     # 保存到文件
q = np.load('p.npy')                    # 从文件读取