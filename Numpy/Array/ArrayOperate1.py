import numpy as np

a = [1, 2, 3, 4]           # 一维数组
b = np.array(a)            # 一维数组的创建
type(b)                  # <type 'numpy.ndarray'>

# print(type(b))

print(b.shape)           # 维度
print(b.argmax())        # 下标
print(b.max())           # 最大值
print(b.mean())          # 平均值