import numpy as np

d = np.array([
    [2, 1],
    [1, 2]
])
"""
对正定矩阵求本征值和本正向量
本征值为u，
本证向量构成的二维array为v，
array([
    [ 0.70710678, -0.70710678],
    [ 0.70710678,  0.70710678]
])
是沿45°方向
eig()是一般情况的本征值分解，对于更常见的对称实数矩阵，
eigh()更快且更稳定，不过输出的值的顺序和eig()是相反的
"""
u, v = np.linalg.eig(d)
u, v = np.linalg.eigh(d)
# print(u, v)

# Cholesky分解并重建
l = np.linalg.cholesky(d)

"""
array([
    [ 2., 1.]
    [ 1., 2.]
])
"""
np.dot(l, l.T)
# print(np.dot(l, l.T))

e = np.array([
    [1, 2],
    [3, 4]
])
# 对不正定矩阵，进行SVD分解并重建
U, s, V = np.linalg.svd(e)

S = np.array([
    [s[0], 0],
    [0, s[1]]
])
print(S)

"""
array([
    [1. 2.]
    [3. 4.]
])
"""
np.dot(U, np.dot(S, V))
print(np.dot(U, np.dot(S, V)))