import numpy as np

"""
求向量的模
"""
a = np.array([3, 4])
np.linalg.norm(a)
# print(np.linalg.norm(a))

b = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
])
c = np.array([1, 0, 1])
# 矩阵和向量的乘法
np.dot(b, c)                    # array([ 4, 10, 16])
# print(np.dot(b, c))
np.dot(c, b.T)                  # array([4, 10, 16])
# print(np.dot(c, b.T))

np.trace(b)                     # 求矩阵的迹
# print(np.trace(b))
np.linalg.det(b)                # 求矩阵的行列式，0
# print(np.linalg.det(b))
np.linalg.matrix_rank(b)        # 求矩阵的秩，不满秩，因为行与行之间等差
print(np.linalg.matrix_rank(b)) # 求矩阵的秩，2，不满秩，因为行与行之间等差