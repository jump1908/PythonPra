# 文件操作
# 推荐用上下文管理器（with-as）来打开文件
# 读取文件内容并全部显示
with open('name_age.txt', 'r') as f:           # 打开文件，读取模式
    lines = f.readlines()                         # 一次性读取所有行
    for line in lines:
        name, age = line.rstrip().split(',')
        print('{} is {} years old'.format(name, age))

# 文件的模式一般有四种，读取(r)，写入(w)，追加(a)和读写(r+)

with open('name_age.txt', 'r') as fread, open('age_name.txt', 'w') as fwrite:
    line = fread.readline()
    while line:
        name, age = line.rstrip().split(',')
        fwrite.write('{},{}\n'.format(age, name))
        line = fread.readline()