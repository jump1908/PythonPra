import pickle

lines = [
    "I'm like a dog chasing cars",
    "I wouldn't know what to do if I caught one...",
    "I'd just do things"
]

with open('lines.pkl', 'wb') as f:          # 序列化并保存成文件
    pickle.dump(lines, f)

with open('lines.pkl', 'rb') as f:          # 从文件读取并反序列化
    lines_back = pickle.load(f)

print(lines_back)                             # 和lines一样