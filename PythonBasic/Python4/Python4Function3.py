# 函数function做为对象使用

def get_val_at_pos_1(x):
    return x[1]

heros = [
    ('Superman', 99),
    ('Batman', 100),
    ('Joker', 85)
]

sorted_pairs0 = sorted(heros, key=get_val_at_pos_1)
sorted_pairs1 = sorted(heros, key=lambda x: x[1])

# lambda Python中的特殊函数，Lambda表达式在Python中是一种匿名函数 比如：
some_ops = lambda x, y: x + y + x*y + x**y
print(some_ops(2, 3))

print(sorted_pairs0)
print(sorted_pairs1)