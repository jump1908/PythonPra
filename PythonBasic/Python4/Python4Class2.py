# 类Class B

from hours_to_learn.Python4.Python4Class1 import A

class B(A):
    """Class B inheritenced from A"""
    def greeting(self):
        print("How's going!")

b = B(12, 12, 'John')
b.introduce()                           # John
b.greeting()                            # How's going!
print(b._name)                          # John
print(b._A__l2norm())                   # "私有"方法，必须通过_A__l2norm访问