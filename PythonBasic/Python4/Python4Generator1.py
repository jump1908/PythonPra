# 生成器Generator1

# 从10倒数到0
def countdown(x):
    while x >= 0:
        yield x
        x -= 1

for i in countdown(10):
    print(i)

# 打印小于100的斐波那契书
def fibonacci(n):
    a = 0
    b = 1
    while b < n:
        yield b
        a, b = b, a+b

for x in fibonacci(100):
    print(x)

# 生成器和所有课迭代结构一样，可以通过next()函数返回下一个值，如果迭代结束了则抛出StopIteration异常：
a = fibonacci(3)
print(next(a))
print(next(a))
print(next(a))
print(next(a))                  # 抛出StopIteration异常