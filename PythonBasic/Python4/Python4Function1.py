# 函数function

def say_hello():
    print('Hello!')

def greetings(x='Good morning'):
    print(x)

say_hello()  # Hello!
greetings()                                         # Good morning!
greetings('Good afternoon')                       # Good afternoon
a = greetings()  # 返回值是None

def create_a_list(x, y=2, z=3):                     #默认参数项必须放在后面
    return [x, y, z]

b = create_a_list(1)                                # [1, 2, 3]
# print(b)
c = create_a_list(3, 3)                             # [3, 3, 3]
# print(c)
d = create_a_list(6, 7, 8)                          # [6, 7, 8]
# print(d)

def traverse_args(*args):
    for arg in args:
        print(arg)

traverse_args(1, 2, 3)                              # 依次打印1，2，3
traverse_args('A', 'B', 'C', 'D')                  # 依次打印A, B, C, D

def traverse_kargs(**kwargs):
    for k, v in kwargs.items():
        print(k, v)

traverse_kargs(x=3, y=4, z=5)                        # 依次打印('x', 3), ('y', 4), ('z', 5)
traverse_kargs(fighter1='Fedor', fighter2='Randleman')

def foo(x, y, *args, **kwargs):
    print(x, y)
    print(args)
    print(kwargs)

# 第一个print输出(1, 2)
# 第二个print输出(3, 4, 5)
# 第三个print输出{'a': 3, 'b': 'bar'}
foo(1, 2, 3, 4, 5, a=6, b='bar')