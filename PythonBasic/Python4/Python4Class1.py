# 类Class A

class A:
    """Class A"""
    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self._name = name

    def introduce(self):
        print(self._name)

    def greeting(self):
        print("What's up!")

    def __l2norm(self):
        return self.x**2 + self.y**2

    def cal_l2norm(self):
        return self.__l2norm()

a = A(11, 11, 'Leonardo')
print(A.__doc__)                            # 'Class A'
a.introduce()                               # "Leonardo"
a.greeting()                                # "What's up"
print(a._name)                              # 可以正常访问
print(a.cal_l2norm())                       # 输出11*11+11*11= 242
print(a._A__l2norm())                       # 仍然可以访问，只是名字不一样，只需要在函数前面加上"_类名"就可以访问
# print(a.__l2norm)                           # 报错：'A' object has no attribute '__l2norm'