# 生成器Generator2

def another_fibonacci(n):
    a = 0
    b = 1
    while b < n:
        yield b
        a, b = b, a+b
    return "No more ....."

a = another_fibonacci(3)
print(next(a))
print(next(a))
print(next(a))
print(next(a))                          #抛出StopIteration异常并打印No more消息