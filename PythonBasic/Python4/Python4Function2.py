# 函数function做为对象使用

moves = ['up', 'left', 'down', 'right']

def move_up(x):                     # 定义向上的操作
    x[1] += 1

def move_down(x):                   # 定义向下的操作
    x[1] -= 1

def move_left(x):                   # 定义向左的操作
    x[1] -= 1

def move_right(x):                  # 定义向右的操作
    x[1] += 1

# 动作和执行的函数关联起来，函数作为键对应的值
actions = {
    'up': move_up,
    'down': move_down,
    'left': move_left,
    'right': move_right
}

coord = [0, 0]

for move in moves:
    actions[move](coord)
    print(coord)