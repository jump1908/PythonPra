#列表1
a = [1, 2, 3, 4]
b = [1]
c = [1]
d = b
e = [1, "Hello World!", c, False]
print(e)
print(id(b),id(c))                          # 35559304 35625608
print(id(b),id(d))                          # 35559304 35559304
print(b==c)                                 # True
f = list('abcd')                           # 利用list函数从任何可遍历结构初始化
print(f)                                    # ['a', 'b', 'c', 'd']
g = [0]*3 + [1]*4 + [2]*2                   # [0, 0, 0, 1, 1, 1, 1, 2, 2]