# 字典1
a = {'Tom': 8, 'Jerry': 7}
print(a['Tom'])                     # 8
b = dict(Tom=8, Jerry=7)            # 一种字符串作为键更方便的初始化方式
print(b['Tom'])                     # 8
if 'Jerry' in a:                    # 判断'Jerry'是否在keys里面
    print(a['Jerry'])               # 7
print(a.get('Spike'))               # None，通过get获得值，即使键不存在也不会报异常
a['Spike'] = 10
a['Tyke'] = 3
a.update({'Tuffy': 2, 'Mammy Two Shoes': 42})
print(a.values())                   # dict_values([8, 7, 10, 3, 2, 42])
print(a.pop('Mammy Two Shoes'))   # 移除'Mammy Two Shoes'的键值对，并返回42
print(a.keys())                     # dict_keys(['Tom', 'Jerry', 'Spike', 'Tyke', 'Tuffy'])