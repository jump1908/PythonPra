# 字典2
a = {'Tom': 8, 'Jerry': 7, 'Spike': 10, 'Tyke': 3, 'Tuffy': 2}         # 初始化a
b = a.items()
print(b)                                # dict_items([('Tom', 8), ('Jerry', 7), ('Spike', 10), ('Tyke', 3), ('Tuffy', 2)])
from operator import itemgetter
c = sorted(a.items(), key=itemgetter(1))
print(c)                                # [('Tuffy', 2), ('Tyke', 3), ('Jerry', 7), ('Tom', 8), ('Spike', 10)]
e = sorted(a)
print(e)                                # 只对键排序， ['Jerry', 'Spike', 'Tom', 'Tuffy', 'Tyke']