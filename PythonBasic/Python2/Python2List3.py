# 列表3
import random
a = list(range(10))                 # 生成一个列表，从0开始+1递增到9
print(a)                            # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
random.shuffle(a)                   # shuffle函数可以对可遍历且可变结构打乱顺序
print(a)                            # [1, 0, 7, 9, 4, 5, 3, 2, 6, 8]
b = sorted(a)
print(b)                            # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
c = sorted(a, reverse=True)
print(c)                            # [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]