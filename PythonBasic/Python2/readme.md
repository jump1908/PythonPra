# Python集合
Python中的容器是异常好用且异常有用的结构。
本文件夹下主要介绍列表（List），元组（tuple），字典（Dict）和集合（Set）。
这些结构和其他语言中的类似结构并无本质不同

## list与tuple的区别
1. 初始化list用[]，tuple用()
2. list可以改变，tuple一经初始化后不可改变，其长度、值都不能发生变化

## list与dict
dict实际上就是哈希表，属于list中的一种，特殊之处在于dict的键值必须是可哈希的

## 总结一下Python 2与Python 3的区别
1. Python2 中range的用法在Python 3中不同，xrange在Python 3中没有体现
2. Python2 中iteritems以及迭代器已经在Python 3中没有体现了