# 元组
a = (1, 2)
# print(a)
b = tuple(['3', 4])                 #也可以从列表初始化
# print(b)
c = (5, )
print(c)
d = (6)                             # 6
print(d)
e = 3, 4, 5                         # (3, 4, 5)
print(e)