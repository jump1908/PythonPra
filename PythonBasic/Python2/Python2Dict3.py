# 列表3
from collections import OrderedDict
a = {1: 2, 3: 4, 5: 6, 7: 8, 9: 10}
b = OrderedDict({1: 2, 3: 4, 5: 6, 7: 8, 9: 10})
print(a)                    # {1: 2, 3: 4, 5: 6, 7: 8, 9: 10}
print(b)                    # OrderedDict([(1, 2), (3, 4), (5, 6), (7, 8), (9, 10)])