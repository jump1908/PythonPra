# 列表2
a = [1, 2, 3, 4]
a.pop()                             # 把最后一个值4从列表中移除并作为pop的返回值
# print(a)
4 in a                              # 判断4是否在a中，False
a.append(5)                         # 末尾插入值，[1. 2. 3. 5]
a[2]                                # 取下标，也就是位置2的值，也就是第三个值3
# print(a[2])
a += [4, 3, 2]                      # 拼接，[1, 2, 3, 5, 4, 3, 2]
a.insert(1, 0)                      # 在下标为1处插入0，[1, 0, 2, 3, 5, 4, 3, 2]
a.remove(2)                         # 移除第一个2，[1, 0, 3, 5, 4, 3, 2]
# print(a)
a.reverse()                         # 倒序，返回值为0，a变为[2, 3, 4, 5, 3, 0, 1]
a[3] = 9                            # 指定下标处赋值，[2, 3, 4, 9, 3, 0, 1]
b = a[2:5]                          # 取下标2开始到5之前的子序列，[4, 9, 3]
# print(b)
c = a[2:-2]                         # 下标也可以很方便地倒着数， -1对应最后一个元素
# print(c)
d = a[2:]                           # 取下标2开始到结尾的子序列， [4, 9, 3, 0, 1]
# print(d)
e = a[:5]                           # 取开始到下标5之前的子序列，[2, 3, 4, 9, 3]
#print(e)
f = a[:]                            # 取从开头到最后的整个子序列，相当于值拷贝，[2, 3, 4, 9, 3, 0, 1]
# print(f)
a[2:-2] = [1, 2, 3]                 # 赋值也可以按照片段来，[2, 3, 1, 2, 3, 0, 1]
# print(a)
g = a[::-1]                         # 也是倒叙，通过slicing实现并赋值，效率略低于reverse()
# print(g)
a.sort()                            # 列表内按数值大小排序
print(a)