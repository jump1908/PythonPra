# 字符串String1

a = 'Life is short, you need Python'
a.lower()                                       # 'life is short, you need python'
a.upper()                                       # 'LIFE IS SHORT, YOU NEED PYTHON'
a.count('i')                                    # 2
a.find('e')                                     # 从左向右查找'e'，3
a.rfind('need')                                 # 从右向左查找'need'，19
a.replace('you', 'I')                           # 'Life is short, I need Python'
tokens = a.split()                               # ['Life', 'is', 'short,', 'you', 'need', 'Python']
b = ' '.join(tokens)                            # 用指定分隔符按顺序把字符串列表组合成新字符串
c = a + '\n'                                    # 加了换行符，注意+用法是字符串作为序列的用法
c.rstrip()                                      # 右侧去除换行符
[x for x in a]                                  # 遍历每个字符并生成右所有字符按顺序构成的列表
'Python' in a                                   # True