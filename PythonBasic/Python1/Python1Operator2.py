#运算符
#逻辑运算符
a = True
b = False
a and b         #False
a or b          #True
not a           #False
#print(a and b )
#print(a or b )
#print(not a)

#位操作
~8              #按位翻转，1000-->-(1000+1)，所得结果为-9
8 >> 3          #右移3位，1000-->0001
#print(8>>3)
1 << 3          #左移3位，0001-->1000
#print(1 << 3 )
5 & 2           #按位与，101&010 = 000
#print(5&2)
5 | 2           #按位或，101|010 = 111
#print(5 | 2 )
4 ^ 1           #按位异或，100^001 = 101
#print(4 ^ 1)

#==，!=和is
'''
==和!=比较引用指向的内存中的内容
而is判断两个变量是否指向一个地址
'''
a = 1
b = 1.0
c = 1
a == b      #True，值相等
#print( a==b )
a is b      #False，指向的不是一个对象，这个语句等效于id(a) = id(b)
#print( a is b )
a is c      #True，指向都是整形值1
#print( a is c )
