#模块导入
'''
import是利用Python中各种强大的库的基础，比如要计算cos(π)的值，可以有下面4种方式
'''
# 直接导入Python的内置基础数学库
import math
print(math.cos(math.pi))

# 从math中导入cos函数和pi变量
from math import cos, pi
print(cos(pi))

# 如果是个模块，在导入的时候可以起个别名，避免名字冲突或是方便懒得打字的人使用
import math as m
print(m.cos(m.pi))

#从math中导入所有东西
from math import *
print(cos(pi))