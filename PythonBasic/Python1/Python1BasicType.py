#基本数据类型
a = 1           #整数
b = 1.2         #浮点数
c = True       #布尔类型
d = "False"    #字符串
e = None        #NoneType

type(a)         #<type 'int'>
type(b)         #<type 'float'>
type(c)         #<type 'bool'>
type(d)         #<type 'str'>
type(d)         #<type 'NoneType'>


print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
