# reduce

from functools import reduce

redobj = reduce(lambda x, y: x+y, [1, 2, 3, 4])
print(redobj)
