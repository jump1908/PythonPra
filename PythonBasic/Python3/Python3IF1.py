# if和分支结构1
pets = ['dog', 'cat', 'droid', 'fly']

for pet in pets:
    if pet == 'dog':
        food = 'steak'
    elif pet == 'cat':
        food = 'milk'
    elif pet == 'droid':
        food = 'oil'
    elif pet == 'fly':
        food == 'sh*t'
    else:
        pass                    # 空语句，什么也不做，占位用
    print(food)