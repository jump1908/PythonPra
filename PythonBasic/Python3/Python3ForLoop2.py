# for循环2
names = ["Rick", "Daryl", "Glenn"]

# 依次输出下标和名字
for i, name in enumerate(names):
    print(i, name)

wusuowei = ["I", "don't", "give", "a", "shit"]      #无所谓

'''
hexie = True
for x in wusuowei:
    if x == "f**k":
        print("what the f**k")
        hexie = False
        break

if hexie:
    print("Harmonious society!")
'''

for x in wusuowei:
    if x == "f**k":
        print("What the f**k")
        hexie = False
        break
else:                                       # for循环中if内语句未被触发
    print("Harmonious society!")