# for循环1
a = ['This', 'is', 'a', 'list', '!']
b = ['This', 'is', 'a', 'tuple', '!']
c = {'This': 'is', 'an': 'unordered', 'dict': '!'}

# 依次输出：'This', 'is', 'a', 'list', '!'
for x in a:
    print(x)

# 依次输出：'This', 'is', 'a', 'tuple', '!'
for x in b:
    print(x)

# 键的遍历。不依次输出：'This', 'dict', 'an'
for key in a:
    print(key)

# 依次输出0到9
for i in range(10):
    print(i)