# if和分支结构2
pets = ['dog', 'cat', 'droid', 'fly']
food_for_pet = {
    'dog': 'steak',
    'cat': 'milk',
    'droid': 'oil',
    'fly': 'sh*t'
}

for pet in pets:
    food = food_for_pet[pet] if pet in food_for_pet else None
    print(food)