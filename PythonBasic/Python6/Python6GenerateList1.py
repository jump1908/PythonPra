# 列表生成1

gl1 = [x**2 for x in [1, 2, 3, 4]]
print(gl1)

gl2 = [sum(x) for x in zip([1, 2, 3], [5, 6, 7])]
print(gl2)

gl3 = [x for x in [1, 2, 3, 4, 5] if x % 2]
print(gl3)